package CommandPatternImplement;

public interface Command {
	public void execute();

	public void undo();
}
