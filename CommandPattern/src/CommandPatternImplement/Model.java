package CommandPatternImplement;

public class Model {
	private boolean playerTurn;
	private int[][] spaces;

	private CommandManager commandManager;

	public Model() {
		spaces = new int[8][8];
		playerTurn = true;
		commandManager = new CommandManager();
	}

	public boolean isPlayerTurn() {
		return playerTurn;
	}

	public void place1(int row, int col) {
		assert (playerTurn);
		assert (spaces[row][col] == 0);
		commandManager.executeCommand(new Place1Command(this, row, col));
	}

	public void place2(int row, int col) {
		assert (!playerTurn);
		assert (spaces[row][col] == 0);
		commandManager.executeCommand(new Place2Command(this, row, col));
	}

	public boolean isSpaceEmpty(int row, int col) {
		return (spaces[row][col] == 0);
	}

	public boolean isSpace1(int row, int col) {
		return (spaces[row][col] == 1);
	}

	public boolean isSpace2(int row, int col) {
		return (spaces[row][col] == 2);
	}

	public boolean hasPlayerWon() {
		// Check rows
		if (spaces[0][0] == 1 && spaces[0][1] == 1 && spaces[0][2] == 1)
			return true;
		if (spaces[1][0] == 1 && spaces[1][1] == 1 && spaces[1][2] == 1)
			return true;
		if (spaces[2][0] == 1 && spaces[2][1] == 1 && spaces[2][2] == 1)
			return true;
		if (spaces[0][0] == 1 && spaces[1][0] == 1 && spaces[2][0] == 1)
			return true;
		if (spaces[0][1] == 1 && spaces[1][1] == 1 && spaces[2][1] == 1)
			return true;
		if (spaces[0][2] == 1 && spaces[1][2] == 1 && spaces[2][2] == 1)
			return true;
		if (spaces[0][0] == 1 && spaces[1][1] == 1 && spaces[2][2] == 1)
			return true;
		if (spaces[0][2] == 1 && spaces[1][1] == 1 && spaces[2][0] == 1)
			return true;
		return false;
	}

	public boolean isGameOver() {
		if (hasPlayerWon())
			return true;
		for (int row = 0; row < 7; row++) {
			for (int col = 0; col < 8; col++) {
				if (spaces[row][col] == 0)
					return false;
			}
		}
		return true;
	}

	public boolean canUndo() {
		return commandManager.isUndoAvailable();
	}

	public void undo() {
		commandManager.undo();
	}

	private class Place1Command implements Command {
		private Model model;
		private int previousValue;
		private boolean previousTurn;
		private int row;
		private int col;

		private Place1Command(Model model, int row, int col) {
			this.model = model;
			this.row = row;
			this.col = col;
			this.previousValue = model.spaces[row][col];
			this.previousTurn = model.playerTurn;
		}

		public void execute() {
			model.spaces[row][col] = 1;
			model.playerTurn = false;
		}

		public void undo() {
			model.spaces[row][col] = previousValue;
			model.playerTurn = previousTurn;
		}
	}

	private class Place2Command implements Command {
		private Model model;
		private int previousValue;
		private boolean previousTurn;
		private int row;
		private int col;

		private Place2Command(Model model, int row, int col) {
			this.model = model;
			this.row = row;
			this.col = col;
			this.previousValue = model.spaces[row][col];
			this.previousTurn = model.playerTurn;
		}

		public void execute() {
			model.spaces[row][col] = 2;
			model.playerTurn = true;
		}

		public void undo() {
			model.spaces[row][col] = previousValue;
			model.playerTurn = previousTurn;
		}
	}

}