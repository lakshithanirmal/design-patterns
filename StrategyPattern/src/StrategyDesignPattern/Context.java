package StrategyDesignPattern;

public class Context {
	private StrategyImplement strategy1 = new ConcreteStrategyX();
	private StrategyImplement strategy2 = new ConcreteStrategyY();
	private StrategyImplement strategy3 = new ConcreteStrategyZ();

	public void execute1() {
		strategy1.executeAlgorithm();
	}

	public void setStrategy1(StrategyImplement strategy1) {
		strategy1 = strategy1;
	}

	public StrategyImplement getStrategy1() {
		return strategy1;
	}
	
	
	
	public void execute2() {
    strategy2.executeAlgorithm();
  }

  public void setStrategy2(StrategyImplement strategy2) {
    strategy2 = strategy2;
  }

  public StrategyImplement getStrategy2() {
    return strategy2;
  }
  
  
  
  public void execute3() {
    strategy3.executeAlgorithm();
  }

  public void setStrategy(StrategyImplement strategy) {
    strategy3 = strategy3;
  }

  public StrategyImplement getStrategy() {
    return strategy3;
  }

}
