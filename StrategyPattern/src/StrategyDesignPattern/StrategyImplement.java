package StrategyDesignPattern;

interface StrategyImplement {
	public void executeAlgorithm();
}

class ConcreteStrategyX implements StrategyImplement {
	public void executeAlgorithm() {
		System.out.println("Concrete Strategy X");
	}
}

class ConcreteStrategyY implements StrategyImplement {
	public void executeAlgorithm() {
		System.out.println("Concrete Strategy Y");
	}
}

class ConcreteStrategyZ implements StrategyImplement {
	public void executeAlgorithm() {
		System.out.println("Concrete Strategy Z");
	}
}
 