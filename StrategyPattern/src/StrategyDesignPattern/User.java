package StrategyDesignPattern;

public class User {

	public static void main(String[] args) {
		Context context1 = new Context();
		context1.execute1();
		context1.setStrategy(new ConcreteStrategyY());
		//context.execute();
		
		
		Context context2 = new Context();
    context2.execute2();
    context2.setStrategy(new ConcreteStrategyY());
    
    
    Context context3 = new Context();
    context3.execute3();
    context3.setStrategy(new ConcreteStrategyY());
	}
}